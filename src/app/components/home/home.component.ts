import { Component, OnInit } from '@angular/core';
import {EventsService} from '../../services/events.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  color = '#e21a1a';

  constructor(private eventsService: EventsService) { }

  ngOnInit(): void {
  }

  changeTheme(themeName: string): void {
    console.log('Theme: ' + themeName);
    const themeAsset = document.getElementById('themeAsset');
    if (themeAsset) {
      // @ts-ignore
      themeAsset.href = `/assets/themes/${themeName}.css`;
      this.eventsService.theme.next(themeName);
    }
  }
}
