import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  NGX_MAT_DATE_FORMATS,
  NgxMatDateFormats,
  NgxMatDatetimePickerModule,
  NgxMatTimepickerModule
} from '@angular-material-components/datetime-picker';
import {ReactiveFormsModule} from '@angular/forms';
import {NgxMatMomentModule} from '@angular-material-components/moment-adapter';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {FormComponent} from './components/form/form.component';
import {HomeComponent} from './components/home/home.component';
import {TabsComponent} from './components/tabs/tabs.component';
import {ButtonsComponent} from './components/buttons/buttons.component';
import {ColorPickerModule} from 'ngx-color-picker';
import {TableComponent} from './components/table/table.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {ToolbarComponent} from './components/toolbar/toolbar.component';

const CUSTOM_DATE_FORMATS: NgxMatDateFormats = {
  parse: {
    dateInput: 'l, LTS'
  },
  display: {
    dateInput: 'l, LTS',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    ButtonsComponent,
    HomeComponent,
    TabsComponent,
    TableComponent,
    ToolbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    ColorPickerModule,
    NgxMaterialTimepickerModule.setLocale('es-ES'),
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatMomentModule
  ],
  providers: [
    { provide: NGX_MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
