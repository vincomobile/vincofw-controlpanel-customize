import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FormComponent} from './components/form/form.component';
import {HomeComponent} from './components/home/home.component';
import {TabsComponent} from './components/tabs/tabs.component';
import {ButtonsComponent} from './components/buttons/buttons.component';
import {TableComponent} from './components/table/table.component';
import {ToolbarComponent} from './components/toolbar/toolbar.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'address', component: FormComponent },
  { path: 'tabs', component: TabsComponent },
  { path: 'table', component: TableComponent },
  { path: 'buttons', component: ButtonsComponent },
  { path: 'toolbar', component: ToolbarComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
